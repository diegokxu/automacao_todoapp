﻿using Desafio_ToDoApp.Objetos;
using Desafio_ToDoApp.Pages;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Desafio_ToDoApp.StepDefinitions
{
    [Binding]
    public class CriarSubTarefasSteps
    {        
        SubTarefaPage subTarefaPage = new SubTarefaPage();       


        [Then(@"Eu vejo o rotulo Manage Subtasks")]
        public void ThenEuVejoORotuloManageSubtasks()
        {
            subTarefaPage.VerificarExistenciaDoBotaoManageSubtasks();
        }
     

        [Given(@"Que eu clico em adicionar uma subtarefa")]
        public void GivenEuClicoEmAdicionarUmaSubtarefa()
        {
            subTarefaPage.ClicarBotaoGerenciarTarefa();
        }

        [Given(@"Tento sobrescrecer o id da tarefa")]
        public void GivenTentoSobrescrecerOIdDaTarefa()
        {
            subTarefaPage.RetornaSeOElemendoEInteragivel();
        }

        [Then(@"O sistema não permite a alteração")]
        public void ThenOSistemaNaoPermiteAAlteracao()
        {
            subTarefaPage.ValidaElementoEEditavel();
        }

        [Given(@"Tento sobrescrecer o descrição da tarefa")]
        public void GivenTentoSobrescrecerODescricaoDaTarefa()
        {

            subTarefaPage.PreencherCampoTarefa();
        }

        [Given(@"Clico em fechar o modal")]
        public void GivenClicoEmFecharOModal()
        {
            subTarefaPage.ClicarBotaoFecharSubTarefa();
        }

        [Then(@"Não permitir atidar a tarefa")]
        public void ThenNaoPermitirAtidarATarefa()
        {
            subTarefaPage.VerificarSeATarefaAlteradaExisteNaTabela();
        }

        [Given(@"Insiro a descrição da subtarefa")]
        public void GivenInsiroADescricaoDaSubtarefa(Table tabela)
        {
            var subTarefa = tabela.CreateInstance<SubTarefas>();
            subTarefaPage.PreencherCampoSubtarefa(subTarefa.Descricao);
        }

        [Given(@"Insiro a descrição da subtarefa com mais de (.*) caracteres")]
        public void GivenInsiroADescricaoDaSubtarefaComMaisDeCaracteres(int p0)
        {
            subTarefaPage.PreencherCampoSubtarefaAleatorio();
        }


        [Given(@"Insiro a data de vencimento subtarefa")]
        public void GivenInsiroADataDeVencimentoSubtarefa(Table tabela)
        {
            var subTarefa = tabela.CreateInstance<SubTarefas>();
            subTarefaPage.PreencherCampoData(subTarefa.Data);            
        }


        [Given(@"Clico em adicionar subtarefa")]
        public void GivenClicoEmAdicionarSubtarefa()
        {
            subTarefaPage.ClicarBotaoAdicionarSubTarefa();
        }       

        [Then(@"Não permitir a inserção da subtarefa")]
        public void ThenNaoPermitirAInsercaoDaSubtarefa()
        {
            subTarefaPage.VerificaANaoCriacaoDeSubTarefaNaTabela();
        }

        [Then(@"Permitir a inserção da subtarefa")]
        public void ThenPermitirAInsercaoDaSubtarefa()
        {
            subTarefaPage.VerificaSeASubTarefaFoiCriadaComSucesso();
        }

        [Given(@"Verifico a quantidade de subtask que existe dentro da tarefa")]
        public void GivenVerificoAQuantidadeDeSubtaskQueExisteDentroDaTarefa()
        {
            subTarefaPage.RetornaQuantidadeDeSubTarefasCriadas();
        }

        [Then(@"Verifico se a quantidade de tarefa é o mesmo que informa o botao")]
        public void ThenVerificoSeAQuantidadeDeTarefaEOMesmoQueInformaOBotao()
        {
            subTarefaPage.VericarQuantidadeDeSubTarefaPelosBotaoDeGerenciamento();
        }
    }
}

