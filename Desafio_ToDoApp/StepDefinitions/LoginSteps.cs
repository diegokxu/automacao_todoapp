﻿using Desafio_ToDoApp.Helpers;
using Desafio_ToDoApp.Pages;
using System;
using TechTalk.SpecFlow;

namespace Desafio_ToDoApp.StepDefinitions {
    [Binding]
    public class LoginSteps {
        LoginPage loginPage = new LoginPage();
        HomePage homePage = new HomePage();

        [Given(@"Logado no sistema")]
        public void GivenLogadoNoSistema()
        {
            DriverFactory.INSTANCE.Navigate().GoToUrl(Properties.Settings.Default.base_url);
            loginPage.PreencherUsuario(Properties.Settings.Default.username);
            loginPage.PreencherSenha(Properties.Settings.Default.password);
            loginPage.ClicarBotaoSignIn();
        }       
    }
}