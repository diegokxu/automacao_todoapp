﻿using Desafio_ToDoApp.Objetos;
using Desafio_ToDoApp.Pages;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Desafio_ToDoApp.StepDefinitions {
    [Binding]
    public class CriarTarefasSteps {
        HomePage homePage = new HomePage();
        MinhaTarefaPage minhaTarefaPage = new MinhaTarefaPage();



        [Given(@"Eu acesso o link My Tasks")]
        public void GivenEuAcessoOLinkMyTasks()
        {
            homePage.AcessarLinkMinhaTarefa();
        }

        [Given(@"Nao informo um nome para a task")]
        public void GivenNaoInformoUmNomeParaATask(Table table)
        {
            var tabela = table.CreateInstance<Tarefas>();
            minhaTarefaPage.PreencherNovaTarefa(tabela.NomeTarefa);
        }

        [When(@"Aperto a tecla Enter")]
        public void WhenApertoATeclaEnter()
        {
            minhaTarefaPage.ClicarTeclaEnter();
        }

        [When(@"Clico em adicionar tarefa")]
        public void WhenClicoEmAdicionarTarefa()
        {
            minhaTarefaPage.ClicarBotaoAddTarefa();
        }


        [Given(@"Faco o Login no sistema")]
        public void GivenFacoOLoginNoSistema()
        {
            homePage.VerificarMensagemDeLoginSucesso();
        }

        [Given(@"Verifico a existe do link My Task")]
        public void GivenVerificoAExisteDoLinkMyTask()
        {
            homePage.AcessarLinkMinhaTarefa();
        }

        [Then(@"Tenho um retorno de sucesso")]
        public void ThenTenhoUmRetornoDeSucesso()
        {
            homePage.RetornaLinkMinhasTarefas();
        }

        [Then(@"Tenho minhas tarefas cadastradas")]
        public void ThenTenhoMinhasTarefasCadastradas()
        {
            minhaTarefaPage.RetornaTarefasCriadas();
        }

        [Then(@"Eu tenho uma mensagem dizendo que estou conectado")]
        public void ThenEuTenhoUmaMensagemDizendoQueEstouConectado()
        {
            minhaTarefaPage.VericarMensagemQueEstaLogadp();
        }

        [Given(@"Preencho o nome da tarefa")]
        public void GivenPreenchoONomeDaTarefa(Table table)
        {
            var tabela = table.CreateInstance<Tarefas>();
            minhaTarefaPage.PreencherNovaTarefa(tabela.NomeTarefa);
        }

        [Given(@"Preencho o nome da tarefa aleatorio")]
        public void GivenPreenchoONomeDaTarefaAleatorio()
        {
            minhaTarefaPage.PreencherNovaTarefa();
        }


        public void GivenPreenchoONomeDaTarefa()
        {
                   
            minhaTarefaPage.PreencherNovaTarefa();
        }

        [Then(@"Cria uma tarefa")]
        public void ThenCriadoUmaTarefa()
        {
            minhaTarefaPage.VerificarSeCriouTarefa();
        }

        [Then(@"Não cria tarefa")]
        public void ThenNaoCriaTarefa()
        {
            minhaTarefaPage.VerificarSeNaoCriouTarefa();
        }

        [Then(@"A tarefa é inserida na tabela")]
        public void ThenATarefaEInseridaNaTabela()
        {
            minhaTarefaPage.VerificaSeATarefaCriadaEstaNaTabela();
        }


    }
}

