﻿using Desafio_ToDoApp.Bases;
using OpenQA.Selenium;

namespace Desafio_ToDoApp.Pages {
    public class LoginPage : PageBase {

        #region Mapeamento e Instacias
        By campoUsuarioEmail = By.Id("user_email");
        By campoSenha = By.Id("user_password");
        By botaoSignIn = By.Name("commit");
        #endregion

        #region SendKeys
        public void PreencherUsuario(string usuario)
        {
            SendKeys(campoUsuarioEmail, usuario);
        }

        public void PreencherSenha(string senha)
        {
            SendKeys(campoSenha, senha);
        }
        #endregion

        #region Clicks
        public void ClicarBotaoSignIn()
        {
            Click(botaoSignIn);
        }
        #endregion
    }
}