﻿using Desafio_ToDoApp.Bases;
using NUnit.Framework;
using OpenQA.Selenium;


namespace Desafio_ToDoApp.Pages {
    public class HomePage : PageBase {

        #region Mapeamento e Instancias
        By mensagemLoginSucesso = By.XPath("//div[@class='alert alert-info']");
        By linkMyTask = By.Id("my_task");
        #endregion

        #region Clicks

        public void AcessarLinkMinhaTarefa()
        {
            Click(linkMyTask);
        }

        #endregion

        #region Asserts

        public void VerificarMensagemDeLoginSucesso()
        {
            Assert.That(GetText(mensagemLoginSucesso).Contains("Signed in successfully."));
        }

        public void RetornaLinkMinhasTarefas()
        {
            Assert.IsTrue(ReturnIfElementIsDisplayed(linkMyTask));
        }
        #endregion
    }
}