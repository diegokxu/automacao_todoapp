﻿using Desafio_ToDoApp.Bases;
using Desafio_ToDoApp.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;


namespace Desafio_ToDoApp.Pages {
    public class MinhaTarefaPage : PageBase {

        #region Mapeamentos e Instancias
        By campoNovaTarefa = By.Id("new_task");
        By botaoAddTarefas = By.XPath("//span [@class='input-group-addon glyphicon glyphicon-plus']");
        By campoNomeListaDeTarefa = By.XPath("//h1[contains(.,'Diego de Oliveira Marinho')]");
        By tabelaDeTarefa = By.XPath("//tbody");
        int quantidadeDeTarefasCriadas = 0;
        string gerarNomeTarefaAleatorio = "Tarefa_" + GeneralHelpers.ReturnStringWithRandomNumbers(2);
        #endregion

        #region SendKeys
        public void PreencherNovaTarefa(string novaTarefa)
        {
            SendKeys(campoNovaTarefa, novaTarefa);
        }

        public void PreencherNovaTarefa()
        {
            SendKeys(campoNovaTarefa, gerarNomeTarefaAleatorio);
        }

        public void ClicarTeclaEnter()
        {
            quantidadeDeTarefasCriadas = RetornaQuantidadeDeTarefasCriadas();
            SendKeysTeclaEnter(campoNovaTarefa);
        }

        #endregion

        #region Clicks
        public void ClicarBotaoAddTarefa()
        {
            quantidadeDeTarefasCriadas = RetornaQuantidadeDeTarefasCriadas();
            Click(botaoAddTarefas);
        }
        #endregion

        #region Retorno
        public string RetornaNomeDaLista()
        {
            return GetText(campoNomeListaDeTarefa);
        }

        public int RetornaQuantidadeDeTarefasCriadas()
        {
            List<string> valores = new List<string>();
            valores = RetornaElemtosDaTabela(tabelaDeTarefa);
            return valores.Count;
        }

        public List<string> RetornaListaDeTarefasCriadas()
        {
            List<string> valores = new List<string>();
            valores = RetornaElemtosDaTabela(tabelaDeTarefa);
            return valores;
        }
        #endregion

        #region Asserts

        public void RetornaTarefasCriadas()
        {
            List<string> valores = new List<string>();
            valores = RetornaElemtosDaTabela(tabelaDeTarefa);
            Assert.IsNotNull(valores);

        }

        public void VericarMensagemQueEstaLogadp()
        {
            Assert.AreEqual(GetText(campoNomeListaDeTarefa), "Hey Diego de Oliveira Marinho, this is your todo list for today:");
        }

        public void VerificarSeCriouTarefa()
        {
            Assert.Less(quantidadeDeTarefasCriadas, RetornaQuantidadeDeTarefasCriadas(), "Tarefa não foi criada");
        }

        public void VerificarSeNaoCriouTarefa()
        {
            Assert.AreEqual(quantidadeDeTarefasCriadas, RetornaQuantidadeDeTarefasCriadas(), "Error foi criado uma tarefa");
        }

        public void VerificaSeATarefaCriadaEstaNaTabela()
        {
            List<string> tarefas = RetornaListaDeTarefasCriadas();
            Assert.AreEqual(gerarNomeTarefaAleatorio, tarefas.Find(x => x.Contains(gerarNomeTarefaAleatorio)));
        }

        #endregion
    }
}