﻿using Desafio_ToDoApp.Bases;
using Desafio_ToDoApp.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;


namespace Desafio_ToDoApp.Pages {
    public class SubTarefaPage : PageBase {

        #region Mapeamento e instancias
        By campoEditarTarefa = By.Id("edit_task");
        By campoNovaSubTarefa = By.Id("new_sub_task");
        By campoDataVencimento = By.Id("dueDate");
        By botaAdicionarSubTarefa = By.Id("add-subtask");
        By rotuloIdTarefas = By.XPath("//h3[@class='modal-title ng-binding']");
        By botaFecharSubTarefa = By.XPath("//button[contains(.,'Close')]");
        By botaoGerenciarSubTarefas = By.XPath("//button [@class='btn btn-xs btn-primary ng-binding']");
        By tabelaDeSubTarefas = By.XPath("//tbody");
        int quantidadeDeSubTarefasCriadas = 0;
        string nomeTarefaAltomatico = "Tarefa_Alterada_" + GeneralHelpers.ReturnStringWithRandomNumbers(2);
        string gerarSubTarefaAleatorio = GeneralHelpers.RetornaStringComCaracteresRandomicos(254);
        MinhaTarefaPage minhaTarefaPage = new MinhaTarefaPage();
        #endregion

        #region SendKeys
        public void PreencherCampoTarefa()
        {
            SendKeys(campoEditarTarefa, nomeTarefaAltomatico);
        }
        public void PreencherCampoSubtarefa(string subTarefa)
        {
            SendKeys(campoNovaSubTarefa, subTarefa);
        }

        public void PreencherCampoData(string data)
        {
            ClearAndSendKeys(campoDataVencimento, data);
        }

        public void PreencherCampoSubtarefaAleatorio()
        {
            SendKeys(campoNovaSubTarefa, gerarSubTarefaAleatorio);
        }

        public void PreencherDataDeVencimento(string data)
        {
            SendKeys(campoDataVencimento, data);
        }
        #endregion

        #region Clicks
        public void ClicarBotaoAdicionarSubTarefa()
        {
            quantidadeDeSubTarefasCriadas = RetornaQuantidadeDeSubTarefasCriadas();
            Click(botaAdicionarSubTarefa);
        }
        public void ClicarBotaoFecharSubTarefa()
        {
            Click(botaFecharSubTarefa);
        }
        public void ClicarBotaoGerenciarTarefa()
        {
            Click(botaoGerenciarSubTarefas);
        }

        #endregion

        #region Retornos
        public List<string> VerificarInsercaoSubTarefaNaTabela()
        {
            List<string> valores = new List<string>();
            valores = RetornaElemtosDaTabelaSubTarefas(tabelaDeSubTarefas);
            return valores;
        }

        public int RetornaQuantidadeDeSubTarefasCriadas()
        {
            List<string> valores = new List<string>();
            valores = RetornaElemtosDaTabelaSubTarefas(tabelaDeSubTarefas);
            quantidadeDeSubTarefasCriadas = valores.Count;
            return valores.Count;
        }

        public bool RetornaSeOElemendoEInteragivel()
        {
            return RetornaSeOElementoEEditavel(rotuloIdTarefas, "Interegivel_" + GeneralHelpers.ReturnStringWithRandomNumbers(2));
        }
        #endregion

        #region Asserts
        public void VerificarSeATarefaAlteradaExisteNaTabela()
        {
            List<string> tarefas = minhaTarefaPage.RetornaListaDeTarefasCriadas();
            Assert.IsFalse(tarefas.Exists(x => x.Contains(nomeTarefaAltomatico)), "Erro A tarefa Alterada Existe na Tabela");
        }

        public void VerificarExistenciaDoBotaoManageSubtasks()
        {
            Assert.IsTrue(ReturnIfElementIsDisplayed(botaoGerenciarSubTarefas));
        }

        public void ValidaElementoEEditavel()
        {
            Assert.IsFalse(RetornaSeOElemendoEInteragivel());
        }

        public void VerificaAusenciaDaSubTarefaNaTabela()
        {
            List<string> tarefas = VerificarInsercaoSubTarefaNaTabela();
            Assert.IsFalse(tarefas.Exists(x => x.Contains(gerarSubTarefaAleatorio)), "Permitiu a inserção de subTarefas");
        }

        public void VerificaSeASubTarefaFoiCriadaComSucesso()
        {
            Assert.Less(quantidadeDeSubTarefasCriadas, RetornaQuantidadeDeSubTarefasCriadas(), "Tarefa não foi criada");

        }

        public void VerificaANaoCriacaoDeSubTarefaNaTabela()
        {
            Assert.AreEqual(quantidadeDeSubTarefasCriadas, RetornaQuantidadeDeSubTarefasCriadas(), "Tarefa foi criada");

        }

        public void VericarQuantidadeDeSubTarefaPelosBotaoDeGerenciamento()
        {
            Assert.AreEqual("(" + quantidadeDeSubTarefasCriadas + ")" + " Manage Subtasks", GetText(botaoGerenciarSubTarefas));
        }

        #endregion
    }
}