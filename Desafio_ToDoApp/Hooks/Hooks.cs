﻿using Desafio_ToDoApp.Helpers;
using TechTalk.SpecFlow;

namespace Desafio_ToDoApp.Hooks {
    [Binding]
    public class Hooks {

        [BeforeScenario]
        public static void BeforeScenario()
        {
            DriverFactory.CreateInstance();
            DriverFactory.INSTANCE.Manage().Window.Maximize();
        }

        [AfterScenario]
        public static void TearDownScenario()
        {
            DriverFactory.QuitInstace();
        }
    }
}