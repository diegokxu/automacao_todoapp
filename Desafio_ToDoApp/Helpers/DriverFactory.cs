﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Desafio_ToDoApp.Helpers {
    public class DriverFactory {
        public static IWebDriver INSTANCE { get; set; } = null;

        public static void CreateInstance()
        {

            if (INSTANCE == null)
            {

                ChromeOptions chrome = new ChromeOptions();
                chrome.AddArgument("start-maximized");
                chrome.AddArgument("enable-automation");
                chrome.AddArgument("--no-sandbox");
                chrome.AddArgument("--disable-infobars");
                chrome.AddArgument("--lang=pt-br");
                chrome.AddArgument("--disable-dev-shm-usage");
                chrome.AddArgument("--disable-browser-side-navigation");
                chrome.AddArgument("--disable-gpu");
                chrome.PageLoadStrategy = PageLoadStrategy.Normal;
                INSTANCE = new ChromeDriver(chrome);
            }
        }

        public static void QuitInstace()
        {
            INSTANCE.Quit();
            INSTANCE.Dispose();
            INSTANCE = null;
        }
    }
}