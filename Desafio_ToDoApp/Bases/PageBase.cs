﻿using Desafio_ToDoApp.Helpers;
using OpenQA.Selenium;
using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Desafio_ToDoApp.Bases {
    public class PageBase {

        #region Objects and constructor
        protected OpenQA.Selenium.Support.UI.WebDriverWait wait { get; private set; }
        protected OpenQA.Selenium.Support.UI.WebDriverWait waitMenor { get; private set; }
        protected IWebDriver driver { get; private set; }
        protected IJavaScriptExecutor javaScriptExecutor { get; private set; }

        public PageBase()
        {
            wait = new OpenQA.Selenium.Support.UI.WebDriverWait(DriverFactory.INSTANCE, TimeSpan.FromSeconds(Convert.ToDouble(Properties.Settings.Default.timeout_default.ToString())));
            waitMenor = new OpenQA.Selenium.Support.UI.WebDriverWait(DriverFactory.INSTANCE, TimeSpan.FromSeconds(Convert.ToDouble(Properties.Settings.Default.timeout_default.ToString())));
            driver = DriverFactory.INSTANCE;
            javaScriptExecutor = (IJavaScriptExecutor)driver;
        }
        #endregion

        #region Custom Actions

        private void WaitUntilPageReady()
        {
            Stopwatch timeOut = new Stopwatch();
            timeOut.Start();

            while (timeOut.Elapsed.Seconds <= Convert.ToInt32(Properties.Settings.Default.timeout_default.ToString()))
            {
                string documentState = javaScriptExecutor.ExecuteScript("return document.readyState").ToString();
                if (documentState.Equals("complete"))
                {
                    timeOut.Stop();
                    break;
                }
            }
        }

        protected IWebElement WaitForElement(By locator)
        {
            WaitUntilPageReady();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(locator));
            IWebElement element = driver.FindElement(locator);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
            return element;
        }

        protected bool RetornaSeOElementoEEditavel(By locator, string text)
        {
            try
            {
                waitMenor.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(locator)).SendKeys(text);
                return true;
            }
            catch (OpenQA.Selenium.ElementNotInteractableException)
            {
                return false;
            }
        }
        protected void Click(By locator)
        {
            Stopwatch timeOut = new Stopwatch();
            timeOut.Start();

            while (timeOut.Elapsed.Seconds <= Convert.ToInt32(Properties.Settings.Default.timeout_default.ToString()))
            {
                try
                {
                    WaitForElement(locator).Click();
                    timeOut.Stop();
                    return;
                }
                catch (System.Reflection.TargetInvocationException)
                {

                }
                catch (StaleElementReferenceException)
                {

                }
                catch (System.InvalidOperationException)
                {

                }
                catch (WebDriverException e)
                {
                    if (e.Message.Contains("Other element would receive the click"))
                    {
                        continue;
                    }

                    if (e.Message.Contains("Element is not clickable at point"))
                    {
                        continue;
                    }

                    throw e;
                }
            }

            throw new Exception("Given element isn't visible");
        }

        protected void SendKeys(By locator, string text)
        {
            WaitForElement(locator).SendKeys(text);
        }

        protected void SendKeysTeclaEnter(By locator)
        {
            driver.FindElement(locator).SendKeys(Keys.Enter);
        }

        protected void ClearAndSendKeys(By locator, String text)
        {
            WaitForElement(locator).Clear();
            WaitForElement(locator).SendKeys(text);
        }

        protected string GetText(By locator)
        {
            string text = WaitForElement(locator).Text;
            return text;
        }

        protected bool ReturnIfElementIsDisplayed(By locator)
        {
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(locator));
            bool result = driver.FindElement(locator).Displayed;
            return result;
        }

        protected List<string> RetornaElemtosDaTabela(By locator)
        {
            IWebElement tabela = driver.FindElement(locator);
            IList<IWebElement> td = tabela.FindElements(By.XPath("//td[@class='task_body col-md-7 limit-word-size']"));
            List<string> valores = new List<string>();
            foreach (IWebElement tarefa in td)
            {
                valores.Add(tarefa.Text);
            }
            return valores;
        }

        protected List<string> RetornaElemtosDaTabelaSubTarefas(By locator)
        {
            IWebElement tabela = driver.FindElement(locator);
            IList<IWebElement> td = tabela.FindElements(By.XPath("//td[@class='task_body col-md-8 limit-word-size']"));
            List<string> valores = new List<string>();
            foreach (IWebElement tarefa in td)
            {
                valores.Add(tarefa.Text);
            }
            return valores;
        }
        #endregion
    }
}