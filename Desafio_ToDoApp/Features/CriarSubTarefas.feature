﻿Feature: Criar SubTarefas
Como usuário do ToDo App
Eu deveria ser capaz de criar uma subtarefa
Para que eu possa dividir minhas tarefas em pedaços menores

Background: Usuario logado no sistema
	Given Logado no sistema
	And Eu acesso o link My Tasks

@Cenario de sucesso
Scenario: Verificar Rotulo Manage Subtasks
	Given Eu acesso o link My Tasks
	Then Eu vejo o rotulo Manage Subtasks

@Cenario de sucesso
Scenario:  Verificar se o botao Manage Subtasks mostra a quantidade de subtarefas existente na tarefa
	Given Que eu clico em adicionar uma subtarefa
	And Verifico a quantidade de subtask que existe dentro da tarefa
	Then Verifico se a quantidade de tarefa é o mesmo que informa o botao

@Cenario de sucesso
Scenario: Verificar campo id se permite apenas leitura
	Given Que eu clico em adicionar uma subtarefa
	And Tento sobrescrecer o id da tarefa
	Then O sistema não permite a alteração

@Cenario de sucesso
Scenario: Verificar campo descrição de tarefa se permite apenas leitura
	Given Que eu clico em adicionar uma subtarefa
	And Tento sobrescrecer o descrição da tarefa
	And Clico em fechar o modal
	Then Não permitir atidar a tarefa

@Cenario de error
Scenario: Inserir descrição de subtarefa com mais de 250 caracteres
	Given Que eu clico em adicionar uma subtarefa
	And Insiro a descrição da subtarefa com mais de 250 caracteres
	And Insiro a data de vencimento subtarefa
		| Data       |
		| 03/10/2019 |
	And Clico em adicionar subtarefa
	Then Não permitir a inserção da subtarefa

@Cenario de sucesso
Scenario: Inserir data e descricao correto
	Given Que eu clico em adicionar uma subtarefa
	And Insiro a descrição da subtarefa
		| Descricao |
		| Algo_task |
	And Insiro a data de vencimento subtarefa
		| Data       |
		| 03/10/2019 |
	And Clico em adicionar subtarefa
	Then Permitir a inserção da subtarefa

	@Cenario de Error
Scenario: Inserir descrição vazio e data correta
	Given Que eu clico em adicionar uma subtarefa
	And Insiro a descrição da subtarefa
		| Descricao |
		|  |
	And Insiro a data de vencimento subtarefa
		| Data       |
		| 03/10/2019 |
	And Clico em adicionar subtarefa
	Then Não permitir a inserção da subtarefa

	@Cenario de Error
Scenario: Inserir descricao correto e data vazio
	Given Que eu clico em adicionar uma subtarefa
	And Insiro a descrição da subtarefa
		| Descricao |
		| Algo2_asd |
	And Insiro a data de vencimento subtarefa
		| Data       |
		|  |
	And Clico em adicionar subtarefa
	Then Não permitir a inserção da subtarefa

	@Cenario de Error
Scenario: Inserir data e descricao vazio
	Given Que eu clico em adicionar uma subtarefa
	And Insiro a descrição da subtarefa
		| Descricao |
		|  |
	And Insiro a data de vencimento subtarefa
		| Data       |
		|  |
	And Clico em adicionar subtarefa
	Then Não permitir a inserção da subtarefa

	@Cenario de Error
Scenario: Inserir descricao correto e data como texto
	Given Que eu clico em adicionar uma subtarefa
	And Insiro a descrição da subtarefa
		| Descricao |
		|  Teste_Descrição|
	And Insiro a data de vencimento subtarefa
		| Data       |
		| Nada a Dizer |
	And Clico em adicionar subtarefa
	Then Não permitir a inserção da subtarefa

	@Cenario de Error
Scenario: Inserir descricao correto e data como caracteres especiais
	Given Que eu clico em adicionar uma subtarefa
	And Insiro a descrição da subtarefa
		| Descricao |
		|  Teste_Descrição|
	And Insiro a data de vencimento subtarefa
		| Data       |
		| @#@!#@!$ |
	And Clico em adicionar subtarefa
	Then Não permitir a inserção da subtarefa

