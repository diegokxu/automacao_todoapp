﻿Feature: CriarTarefas
Como usuário do ToDo App
Eu deveria ser capaz de criar uma tarefa
Para que eu possa gerenciar minhas tarefas

Background: Usuario logado no sistema
	Given Logado no sistema

@CenarioDeSucesso
Scenario: Verificar a existencia do link My Tasks
	Given Faco o Login no sistema
	And Verifico a existe do link My Task
	Then Tenho um retorno de sucesso

@CenarioDeSucesso
Scenario:  Verificar Se todas as tarefas esta listada
	Given Eu acesso o link My Tasks
	Then Tenho minhas tarefas cadastradas

@CenarioDeSucesso
Scenario:Verificar a existencia da mensagem na parte superior dizendo que a lista pertence ao usuário conectado
	Given Eu acesso o link My Tasks
	Then Eu tenho uma mensagem dizendo que estou conectado

@CenarioDeSucesso
Scenario: Criar tarefa Cenario de sucesso
	Given Eu acesso o link My Tasks
	And  Preencho o nome da tarefa
		| NomeTarefa         |
		| Nova tarefa criada |
	When Aperto a tecla Enter
	Then Cria uma tarefa

@CenarioDeSucesso
Scenario: Criar tarefa Cenario de sucesso usando Botão adicionar tarefa
	Given Eu acesso o link My Tasks
	And  Preencho o nome da tarefa
		| NomeTarefa         |
		| Nova tarefa criada |
	When Clico em adicionar tarefa
	Then Cria uma tarefa

@CenarioDeErro
Scenario: Criar tarefa com dois caracteres
	Given Eu acesso o link My Tasks
	And  Preencho o nome da tarefa
		| NomeTarefa |
		| No         |
	When Aperto a tecla Enter
	Then Não cria tarefa

@CenarioDeErro
Scenario: Criar tarefa com nome vazio
	Given Eu acesso o link My Tasks
	And  Preencho o nome da tarefa
		| NomeTarefa |
		|            |
	When Aperto a tecla Enter
	Then Não cria tarefa

@CenarioDeErro
Scenario: Criar tarefa com mais de 250 caracteres
	Given Eu acesso o link My Tasks
	And  Preencho o nome da tarefa
		| NomeTarefa                                                                                                                                                                                                                                                     |
		| verificar a inserção com mais de 250 caracteres na criação de tarefas.verificar a inserção com mais de 250 caracteres na criação de tarefas.verificar a inserção com mais de 250 caracteres na criação de tarefas.verificar a inserção com mais de 250 caracte |
	When Aperto a tecla Enter
	Then Não cria tarefa

@CenarioDeSucesso
Scenario: Verificar se a tarefa foi adicionada na lista
	Given Eu acesso o link My Tasks
	And  Preencho o nome da tarefa aleatorio
	When Clico em adicionar tarefa
	Then A tarefa é inserida na tabela